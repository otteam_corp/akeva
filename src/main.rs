extern crate akeva_reader;
extern crate akeva_writer;

use std::env;
use akeva_reader::get_keys_and_values;
use akeva_writer::insert_key_and_value;


fn main() {
    
    let args: Vec<String> = env::args().collect();
    
    if args.len() <= 3 {
        panic!("Number of parameters uncorrect");
    }

    let path_argument = &args[1];
    let key_to_search_argument = &args[2];
    let value_to_insert = &args[3];

    insert_key_and_value(path_argument, key_to_search_argument, value_to_insert);
    get_keys_and_values(path_argument, key_to_search_argument);
}
