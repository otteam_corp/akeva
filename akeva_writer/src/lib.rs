use std::path::Path;
use std::fs::OpenOptions;
use std::io::Write;

/**
 * Function to insert the tuple (key,value) inside the database file
 */
pub fn insert_key_and_value(path_to_database: &String, key: &String, value: &String){
    
    // create a path to the desired file
    let path = Path::new(&path_to_database);

    // open the file or create it to append info
    match OpenOptions::new().write(true)
                            .append(true)
                            .create(true)
                            .open(path){
                                Err(why) => panic!("Couldn't open {} : {}", path.display(), why.to_string()),
                                Ok(mut file) =>{ 
                                    let key_value = format!("{}\n", key.to_owned() +"#"+value);
                                    file.write_all(key_value.as_bytes())
                                    },
                            }.unwrap();    
}
