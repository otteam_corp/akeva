#[macro_use]
extern crate lazy_static;

use std::fs::File;
use std::path::Path;
use std::io::Read;
use regex::Regex;

/**
 * Function to get the keys and values of the database
 */
pub fn get_keys_and_values(path_to_database: &String, key_to_search: &String){

    lazy_static! { 
        static ref REGULAR_EXPRESSION_LINE: Regex = Regex::new(r"\d#.*").unwrap();
        static ref REGULAR_EXPRESSION_KEY: Regex = Regex::new(r"^\d#").unwrap();
        static ref REGULAR_EXPRESSION_KEY_FORMATTED: Regex = Regex::new(r"\d*").unwrap();
        static ref REGULAR_EXPRESSION_VALUE: Regex = Regex::new(r"#.*$").unwrap();
    }

    // create a path to the desired file
    let path = Path::new(&path_to_database);
    let display = path.display();

    // open the file
    let mut file = match File::open(&path){
        Err(why) => panic!("couldn't open {}: {}", display, why.to_string()),
        Ok(file) => file
    };

    // read the file and match the key 
    let mut content = String::new();
    match file.read_to_string(&mut content){
        Err(why) => panic!("couldn't read {}: {}", display, why.to_string()),
        Ok(_) => 
        {
            for cap in REGULAR_EXPRESSION_LINE.captures_iter(&*content){
                
                let key_capture = REGULAR_EXPRESSION_KEY.captures(&cap[0]).unwrap();
                let key_formatted_capture = REGULAR_EXPRESSION_KEY_FORMATTED.captures(&key_capture[0]).unwrap();
                let value_capture = REGULAR_EXPRESSION_VALUE.captures(&cap[0]).unwrap();
                
                let key_formated_str = key_formatted_capture.get(0).map_or("", |m| m.as_str());
                let value_str = value_capture.get(0).map_or("", |m| m.as_str()); 
                 
                if key_formated_str == key_to_search {
                    println!("{}", &value_str[1..]);
                } 
            }
        }        
    }
}
